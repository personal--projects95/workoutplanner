package com.example.WorkoutPlanner.Exercises;

import javax.persistence.*;
import java.time.LocalDate;


@Entity
@Table
public class Exercises {
    @Id
    @SequenceGenerator(
            name = "exercises_sequence",
            sequenceName = "exercises_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "exercises_sequence"
    )
    private Long id;
    private String username;
    private String exercise;
    private Integer sets;
    private Integer reps;
    private LocalDate date;
    private String tag;

    public Exercises() {
    }

    public Exercises(Long id, String username, String exercise, Integer sets, Integer reps, LocalDate date, String tag) {
        this.id = id;
        this.username = username;
        this.exercise = exercise;
        this.sets = sets;
        this.reps = reps;
        this.date = date;
        this.tag = tag;
    }

    public Exercises(String username, String exercise, Integer sets, Integer reps, LocalDate date, String tag) {
        this.username = username;
        this.exercise = exercise;
        this.sets = sets;
        this.reps = reps;
        this.date = date;
        this.tag = tag;
    }

    public Exercises(String username, String exercise, Integer sets, Integer reps, LocalDate date) {
        this.username = username;
        this.exercise = exercise;
        this.sets = sets;
        this.reps = reps;
        this.date = date;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getExercise() {
        return exercise;
    }

    public void setExercise(String exercise) {
        this.exercise = exercise;
    }

    public Integer getSets() {
        return sets;
    }

    public void setSets(Integer sets) {
        this.sets = sets;
    }

    public Integer getReps() {
        return reps;
    }

    public void setReps(Integer reps) {
        this.reps = reps;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Exercises{" +
                "username='" + username + '\'' +
                ", exercise='" + exercise + '\'' +
                ", sets=" + sets +
                ", reps=" + reps +
                ", date=" + date +
                '}';
    }
}
