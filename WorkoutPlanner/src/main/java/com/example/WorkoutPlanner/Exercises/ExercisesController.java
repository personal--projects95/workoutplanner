package com.example.WorkoutPlanner.Exercises;

import com.example.WorkoutPlanner.Users.UsersController;
import com.example.WorkoutPlanner.Users.UsersUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;



@Controller
public class ExercisesController {

    private final ExercisesService exercisesService;
    Random rand = new Random();
    public String user;

    ExercisesUtils usersUtils = new ExercisesUtils();

    @Autowired
    public ExercisesController(ExercisesService exercisesService) {
        this.exercisesService = exercisesService;
    }


    @PostMapping("/user/add-exercises")
    public String submitExercise(@ModelAttribute Exercises exercise, Model model, HttpSession session, RedirectAttributes redirAttrs) {
        model.addAttribute("addExercise", exercise);
        user = (String) session.getAttribute("name");
        exercise.setUsername(user);
        if (exercise.getExercise() == "" || exercise.getReps() == null || exercise.getSets() == null){
            redirAttrs.addFlashAttribute("statusMessage", "One or more fields are empty. Please, fill them before submitting!");
            return "redirect:/user/add-exercises";
        }
        exercisesService.addExercise(exercise);
        redirAttrs.addFlashAttribute("statusMessage", "Adding completed successfully!");

        return "redirect:/user/add-exercises";

    }

    @GetMapping("/exercises/history")
    public String exercisesSearch(@ModelAttribute Exercises exercises, Model model, HttpSession session){
        user = (String) session.getAttribute("name");
        List<Exercises> contentReceived = exercisesService.getExercises(user);
        model.addAttribute("contentReceived", contentReceived);
        return "history";
    }

    @GetMapping("/exercises/generate-workout")
    public String generateWorkout(Model model) {
        return "generateWorkout";
    }

    @GetMapping("/exercises/generate")
    public String generateOption1(Model model, @RequestParam String type){

        if (type.equals("allBody")) {
            List generateList = new ExercisesUtils(exercisesService).generate("back", "biceps", 2);
            generateList.addAll(new ExercisesUtils(exercisesService).generate("chest", "triceps", 2));
            generateList.addAll(new ExercisesUtils(exercisesService).generate("abs", "legs", 2));
            model.addAttribute("contentReceived", generateList);
            return "option1";
        } else if (type.equals("back-biceps")) {
            model.addAttribute("contentReceived", new ExercisesUtils(exercisesService).generate("back", "biceps", 6));
            return "option3";
        } else if (type.equals("chest-triceps")) {
            model.addAttribute("contentReceived", new ExercisesUtils(exercisesService).generate("chest", "triceps", 6));
            return "option2";
        }

        model.addAttribute("contentReceived", new ExercisesUtils(exercisesService).generate("abs", "legs", 6));
        return "option4";
    }

    /*@GetMapping("/exercises/generate/back-biceps")
    public String generateOption3(Model model){

        ExercisesUtils exercisesUtils = new ExercisesUtils(exercisesService);
        List generateWorkout = exercisesUtils.generateBackBiceps();
        model.addAttribute("contentReceived", generateWorkout);

        return "option3";
    }

    @GetMapping("/exercises/generate/allBody")
    public String generateOption1(Model model){

        ExercisesUtils exercisesUtils = new ExercisesUtils(exercisesService);
        List generateWorkout = exercisesUtils.generateAllBody();
        model.addAttribute("contentReceived", generateWorkout);

        return "option1";
    }

    @GetMapping("/exercises/generate/chest-triceps")
    public String generateOption2(Model model){

        ExercisesUtils exercisesUtils = new ExercisesUtils(exercisesService);
        List generateWorkout = exercisesUtils.generateChestTriceps();
        model.addAttribute("contentReceived", generateWorkout);

        return "option2";
    }

    @GetMapping("/exercises/generate/legs-abs")
    public String generateOption4(Model model){

        ExercisesUtils exercisesUtils = new ExercisesUtils(exercisesService);
        List generateWorkout = exercisesUtils.generateLegsAbs();
        model.addAttribute("contentReceived", generateWorkout);

        return "option4";
    }*/
}
