package com.example.WorkoutPlanner.Exercises;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Service
public class ExercisesUtils {

    List<Exercises> workoutGenerated = new ArrayList<Exercises>();
    private ExercisesService exercisesService ;
    Random rand = new Random();
    @Autowired
    public ExercisesUtils(ExercisesService exercisesService) {
        this.exercisesService = exercisesService;
    }

    public ExercisesUtils() {
    }

    List generate(String groupOne, String groupTwo,  Integer number){

        List<Exercises> exercisesGroupOne = exercisesService.getExercisesWorkout(groupOne);
        List<Exercises> exercisesGroupTwo = exercisesService.getExercisesWorkout(groupTwo);
        int exGenerated = 0, noRandom = 0;
        do {
            noRandom = rand.nextInt(exercisesGroupOne.size());
            workoutGenerated.add(exercisesGroupOne.get(noRandom));
            exercisesGroupOne.remove(noRandom);
            ++exGenerated;
            noRandom = rand.nextInt(exercisesGroupTwo.size());
            workoutGenerated.add(exercisesGroupTwo.get(noRandom));
            exercisesGroupTwo.remove(noRandom);
            ++exGenerated;
        }
        while (exGenerated < number);

        return workoutGenerated;
    }   

}
