package com.example.WorkoutPlanner.Exercises;

import com.example.WorkoutPlanner.Users.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ExercisesRepository extends JpaRepository <Exercises, Long> {
    @Query("SELECT e FROM Exercises e WHERE e.username = ?1")
    List<Exercises> findByUsername(String username);

    @Query("SELECT e FROM Exercises e WHERE e.tag = ?1")
    List<Exercises> findExercises(String tag);
}
