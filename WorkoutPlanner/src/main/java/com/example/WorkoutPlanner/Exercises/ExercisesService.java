package com.example.WorkoutPlanner.Exercises;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class ExercisesService {
    private final ExercisesRepository exercisesRepository;

    @Autowired
    public ExercisesService(ExercisesRepository exercisesRepository) {
        this.exercisesRepository = exercisesRepository;
    }

    public void addExercise (Exercises exercises) {
        exercises.setDate(LocalDate.now());
        exercisesRepository.save(exercises);
    }

    public List<Exercises> getExercises(String username) {
        return exercisesRepository.findByUsername(username);
    }

    public List<Exercises> getExercisesWorkout(String tag) {
        return exercisesRepository.findExercises(tag);
    }

}
