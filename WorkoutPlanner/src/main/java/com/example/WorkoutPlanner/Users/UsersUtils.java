package com.example.WorkoutPlanner.Users;

import org.springframework.web.servlet.mvc.support.RedirectAttributes;

public class UsersUtils {

    public UsersUtils() {
    }

    Boolean notLogged (String name, RedirectAttributes redirAttrs){

        if (name == null){
            redirAttrs.addFlashAttribute("errorMessage","You are not logged-in. Please, sign-in!" );
            return true;
        }
        return false;
    }
}
