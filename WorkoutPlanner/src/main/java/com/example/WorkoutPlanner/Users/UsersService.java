package com.example.WorkoutPlanner.Users;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsersService {

    private final UsersRepository usersRepository;
    @Autowired
    public UsersService(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    public List<Users> getUsers(String username) {
        return usersRepository.findByUsername(username);
    }

    public String getPass (String username){return usersRepository.getPass(username);}

    public String getName (String username){return usersRepository.getName(username);}

    public void addUsers (Users users) {
        usersRepository.save(users);
    }
}
