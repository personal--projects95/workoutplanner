package com.example.WorkoutPlanner.Users;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface UsersRepository extends JpaRepository<Users, Long> {

    @Query("SELECT u FROM Users u WHERE u.username = ?1")
    List<Users> findByUsername(String username);

    @Query("SELECT password FROM Users u WHERE u.username = ?1")
    String getPass(String username);

    @Query("SELECT firstName FROM Users u WHERE u.username = ?1")
    String getName(String username);
}
