package com.example.WorkoutPlanner.Users;

import com.example.WorkoutPlanner.Exercises.Exercises;
import com.example.WorkoutPlanner.Exercises.ExercisesUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class UsersController {

    private final UsersService usersService;
    public String user;

    public String name;
    UsersUtils usersUtils = new UsersUtils();
    @Autowired
    public UsersController(UsersService usersService) {
        this.usersService = usersService;
    }


    @GetMapping("/user/add-exercises")
    public String contentForm(Model model, HttpSession session) {
        model.addAttribute("addExercise", new Exercises());
        session.setAttribute("name", name);
        if (name.equals("admin")){
            return "addExerciseAdmin";
        }
        return "addExercise";
    }

    @GetMapping("/")
    public String usersRegister(Model model) {
        model.addAttribute("registration", new Users());
        return "register";
    }

    @GetMapping("/user/login")
    public String usersLogin(Model model) {
        model.addAttribute("login", new Users());
        name = null;
        return "login";
    }


    @GetMapping("/user/menu")
    public String main(Model model, RedirectAttributes redirAttrs) {

        List<Users> username = usersService.getUsers(name);
        model.addAttribute("nume", name);
        if (name == null) {
            redirAttrs.addFlashAttribute("errorMessage","You are not logged-in. Please, sign-in!" );
            return "redirect:/user/login";
        }
        return "main";
    }

    @GetMapping("/user/login/error")
    public String usersConnect(@ModelAttribute Users user, Model model, RedirectAttributes redirAttrs) {
        model.addAttribute("login", user);
        List<Users> userSearched = usersService.getUsers(user.getUsername());
        String pass = usersService.getPass(user.getUsername());
        String username = user.getUsername();
        name = username;
        if ((user.getUsername() == "") || (user.getPassword() == "")){
            redirAttrs.addFlashAttribute("errorMessage", "One or both fields are empty! Please, fill them in!");
            return "redirect:/user/login";
        } else if (userSearched.isEmpty()){
            redirAttrs.addFlashAttribute("errorMessage", "The username doesn't exists!");
            return "redirect:/user/login";
        } else if (!user.getPassword().equals(pass)) {
            redirAttrs.addFlashAttribute("errorMessage", "Incorrect password!");
            return "redirect:/user/login";
        }
        redirAttrs.addFlashAttribute("errorMessage",name);
        return "redirect:/user/menu";
    }

    @PostMapping("/")
    public String usersSubmit(@ModelAttribute Users users, Model model, RedirectAttributes redirAttrs){
        model.addAttribute("registration", users);
        List<Users> verifyUsers = usersService.getUsers(users.getUsername());

        if ((users.getUsername() == "") || (users.getPassword() == "") || (users.getFirstName() == "") ||
                (users.getLastName() == "") || (users.getPassword1() == ""))  {
            redirAttrs.addFlashAttribute("errorMessage", "One or more fields are empty! Please, fill them in!");
            return "redirect:/";
        } else if (!users.getPassword().equals(users.getPassword1())) {
            redirAttrs.addFlashAttribute("errorMessage", "Different passwords! Verify them!");
            return "redirect:/";
        } else if (!verifyUsers.isEmpty()) {
            redirAttrs.addFlashAttribute("errorMessage", "Username already existing! Choose another one!");
            return "redirect:/";
        } else {
            usersService.addUsers(users);
        }
        redirAttrs.addFlashAttribute("errorMessage", "Registration done successfully! ");
        return "redirect:/";

    }

    @GetMapping("/user/exercises-description")
    public String exercisesDescription(RedirectAttributes redirAttrs) {

        if (usersUtils.notLogged(name, redirAttrs)){
             return "redirect:/user/login";
        }
        return "exercisesDescription";
    }

    @GetMapping("/user/exercise-description/barbellCurl")
    public String barbellCurl(RedirectAttributes redirAttrs) {
        if (usersUtils.notLogged(name, redirAttrs)){
            return "redirect:/user/login";
        }
        return "barbellCurl";
    }

    @GetMapping("/user/exercise-description/cableChestFlys")
    public String cableChestFlys(RedirectAttributes redirAttrs) {
        if (usersUtils.notLogged(name, redirAttrs)){
            return "redirect:/user/login";
        }
        return "cableChestFlys";
    }

    @GetMapping("/user/exercise-description/diamondPushUps")
    public String diamondPushUps(RedirectAttributes redirAttrs) {
        if (usersUtils.notLogged(name, redirAttrs)){
            return "redirect:/user/login";
        }
        return "diamondPushUps";
    }

    @GetMapping("/user/exercise-description/EZ-Bar")
    public String EZBar(RedirectAttributes redirAttrs) {
        if (usersUtils.notLogged(name, redirAttrs)){
            return "redirect:/user/login";
        }
        return "EZ-BarPreacherCurl";
    }

    @GetMapping("/user/exercise-description/gripChestPress")
    public String gripChestPress(RedirectAttributes redirAttrs) {
        if (usersUtils.notLogged(name, redirAttrs)){
            return "redirect:/user/login";
        }
        return "gripChestPress";
    }

    @GetMapping("/user/exercise-description/hammerCurl")
    public String hammerCurl(RedirectAttributes redirAttrs) {
        if (usersUtils.notLogged(name, redirAttrs)){
            return "redirect:/user/login";
        }
        return "hammerCurl";
    }

    @GetMapping("/user/exercise-description/legExtension")
    public String legExtension(RedirectAttributes redirAttrs) {
        if (usersUtils.notLogged(name, redirAttrs)){
            return "redirect:/user/login";
        }
        return "legExtension";
    }

    @GetMapping("/user/exercise-description/machineChestPress")
    public String machineChestPress(RedirectAttributes redirAttrs) {
        if (usersUtils.notLogged(name, redirAttrs)){
            return "redirect:/user/login";
        }
        return "machineChestPress";
    }

    @GetMapping("/user/exercise-description/overheadTricepsExtensions")
    public String overheadTricepsExtensions(RedirectAttributes redirAttrs) {
        if (usersUtils.notLogged(name, redirAttrs)){
            return "redirect:/user/login";
        }
        return "overheadTricepsExtensions";
    }

    @GetMapping("/user/exercise-description/romanianDeadlift")
    public String romanianDeadlift(RedirectAttributes redirAttrs) {
        if (usersUtils.notLogged(name, redirAttrs)){
            return "redirect:/user/login";
        }
        return "romanianDeadlift";
    }

    @GetMapping("/user/exercise-description/ropePushDowns")
    public String ropePushDowns(RedirectAttributes redirAttrs) {
        if (usersUtils.notLogged(name, redirAttrs)){
            return "redirect:/user/login";
        }
        return "ropePushDowns";
    }

   @GetMapping("/user/exercise-description/squat")
    public String squat(RedirectAttributes redirAttrs) {
        if (usersUtils.notLogged(name, redirAttrs)){
            return "redirect:/user/login";
        }
        return "squat";
    }

}
